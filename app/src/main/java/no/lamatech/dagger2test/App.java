package no.lamatech.dagger2test;

import android.app.Application;
import android.content.Context;

import no.lamatech.commonlib.CommonModule;
import no.lamatech.lib1.Lib1Module;
import no.lamatech.lib2.Lib2Module;

public class App extends Application {
    private AppComponent component;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createGraph();
    }

    public AppComponent component() {
        return component;
    }

    private void createGraph() {
        final AppModule appModule = new AppModule(this);
        final Lib1Module lib1Module = new Lib1Module();
        final Lib2Module lib2Module = new Lib2Module();
        final CommonModule commonModule = new CommonModule();

        component = DaggerAppComponent.builder()
                .appModule(appModule)
                .lib1Module(lib1Module)
                .lib2Module(lib2Module)
                .commonModule(commonModule)
                .build();

        component.instantiateLib1Component(lib1Module, commonModule);
    }
}
