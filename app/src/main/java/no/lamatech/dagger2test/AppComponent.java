package no.lamatech.dagger2test;

import javax.inject.Singleton;

import dagger.Component;
import no.lamatech.commonlib.CommonModule;
import no.lamatech.lib1.Lib1Component;
import no.lamatech.lib1.Lib1Module;
import no.lamatech.lib2.Lib2Module;

@Singleton
@Component(
        modules = {
                AppModule.class,
                Lib1Module.class,
                Lib2Module.class,
                CommonModule.class
        }

)
public interface AppComponent {
    void inject(MainActivity activity);

    Lib1Component instantiateLib1Component(Lib1Module lib1Module, CommonModule commonModule);
}
