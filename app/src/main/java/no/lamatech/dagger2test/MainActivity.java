package no.lamatech.dagger2test;

import android.app.Application;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import no.lamatech.commonlib.CommonObj;
import no.lamatech.lib1.Lib1Obj;
import no.lamatech.lib2.Lib2Obj;

public class MainActivity extends AppCompatActivity {
    @Inject Application application;
    @Inject Lib2Obj lib2Obj;
    @Inject Lib1Obj lib1Obj;
    @Inject CommonObj commonObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get(this).component().inject(this);
        setContentView(R.layout.activity_main);
    }
}
