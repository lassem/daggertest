package no.lamatech.lib1;

import dagger.Module;
import dagger.Provides;

@Module
public class Lib1Module {
    public Lib1Module() {
    }

    @Provides
    Lib1Obj provideLib1Obj() {
        return new Lib1Obj();
    }
}
