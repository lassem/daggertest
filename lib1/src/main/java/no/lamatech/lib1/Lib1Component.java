package no.lamatech.lib1;

import dagger.Subcomponent;
import no.lamatech.commonlib.CommonModule;

@Subcomponent(modules = {
        Lib1Module.class,
        CommonModule.class
})
public abstract class Lib1Component {
    private static Lib1Component INSTANCE = null;

    public static Lib1Component component() {
        return INSTANCE;
    }

    public Lib1Component() {
        INSTANCE = this;
    }

    public abstract void inject(Lib1Obj lib1Obj);
}
