package no.lamatech.lib1;

import javax.inject.Inject;

import no.lamatech.commonlib.CommonObj;

public class Lib1Obj {
    @Inject CommonObj commonObj;

    public Lib1Obj() {
        Lib1Component.component().inject(this);
    }
}
