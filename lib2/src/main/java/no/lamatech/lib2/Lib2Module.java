package no.lamatech.lib2;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class Lib2Module {

    @Provides
    Lib2Obj provideLib2Obj() {
        return new Lib2Obj();
    }
}
