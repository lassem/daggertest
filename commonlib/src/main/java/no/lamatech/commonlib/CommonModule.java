package no.lamatech.commonlib;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CommonModule {

    @Provides
    @Singleton
    CommonObj provideCommonObj() {
        return new CommonObj();
    }
}
