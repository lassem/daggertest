package no.lamatech.commonlib;

import dagger.Subcomponent;

@Subcomponent(modules = {
        CommonModule.class
})
public interface CommonComponent {
}
